/**
 * 
 */

/**
 * @author Kleeberg
 *
 */
import java.util.Scanner;
import java.lang.Math;

public class Fahrkartenautomat_Methoden
{
    public static void main(String[] args)
    
    {
      
       double zuZahlenderBetrag1 = fahrkatenbestellungErfassen();       
       double r�ckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag1);

       fahrkartenAusgeben();
       rueckgeldAusgeben(r�ckgabebetrag);

    }
    
    public static double fahrkatenbestellungErfassen() {
    	Scanner myScanner = new Scanner(System.in);
    	System.out.println("Ticketpreis in Euro: ");
    	double ticketPreis = myScanner.nextDouble();
    	System.out.println("Anzahl der Tickets: ");
    	int anzahlTickets = myScanner.nextInt();
    	double gesamtpreis = ticketPreis*anzahlTickets;
    	return gesamtpreis;   	
    }
    public static double fahrkartenBezahlen(double zuZahlen) {
    	Scanner myScanner = new Scanner(System.in);
        double eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlen)
        {
     	   System.out.printf("Noch zu zahlen: " + "%.2f", (zuZahlen - eingezahlterGesamtbetrag), " Euro");
     	   System.out.print("\nEingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   double eingeworfeneM�nze = myScanner.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
        }
        double r�ckgabebetrag = (eingezahlterGesamtbetrag - zuZahlen);
        return r�ckgabebetrag;
    }
    public static void fahrkartenAusgeben() {
	    System.out.println("\nFahrschein wird ausgegeben");
	    for (int i = 0; i < 8; i++)
	    {
	       System.out.print("=");
	       warte(250);
	    }
	    System.out.println("\n\n");
    }
    public static void warte(int millisekunde) {
		  	try {
				Thread.sleep(millisekunde);
		  	} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	
    }
    public static void muenzeAusgeben(double rueckgabebetrag, String einheit) {
    	System.out.println(rueckgabebetrag + " " + einheit);
    }
    public static void rueckgeldAusgeben(double rueckgabebetrag) {
    	
        if(rueckgabebetrag > 0.0)
        {
     	   System.out.printf("Der R�ckgabebetrag in H�he von " + "%.2f", rueckgabebetrag);
     	   System.out.println(" Euro");
     	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while(rueckgabebetrag >= 1.99) // 2 EURO-M�nzen = 200 CENT
            {
         	  muenzeAusgeben(rueckgabebetrag, "Euro");
         	  rueckgabebetrag -= 2;
            }
            while(rueckgabebetrag >= 0.99) // 1 EURO-M�nzen = 100 CENT
            {
              muenzeAusgeben(rueckgabebetrag, "Euro");
         	  rueckgabebetrag -= 1;
            }
            while(rueckgabebetrag >= 0.49) // 50 CENT-M�nzen 
            {
              muenzeAusgeben(rueckgabebetrag, "Cent");
         	  rueckgabebetrag -= 0.5;
            }
            while(rueckgabebetrag >= 0.19) // 20 CENT-M�nzen 
            {
              muenzeAusgeben(rueckgabebetrag, "Cent");
         	  rueckgabebetrag -= 0.20;
            }
            while(rueckgabebetrag >= 0.09) // 10 CENT-M�nzen
            {
              muenzeAusgeben(rueckgabebetrag, "Cent");
         	  rueckgabebetrag -= 0.10;
            }
            while(rueckgabebetrag >= 0.04)// 5 CENT-M�nzen
            {
              muenzeAusgeben(rueckgabebetrag, "Cent");
         	  rueckgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir w�nschen Ihnen eine gute Fahrt.");
    }
    
    
    
}