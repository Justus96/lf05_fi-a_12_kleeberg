public class Variablen{   
	public static void main(String[] args) {
        
        int zahl1 = 7, zahl2 = -3;
        double zahl3 = 5.0, zahl4 = -9.0, erg = 0.0;
        
        erg = methode(zahl1, zahl3);
        erg = methode(zahl2, erg);
                
        System.out.println(erg);        
    }
    
    public static double methode(int z1, double z2) {    
       return z1+z2+1;
    }
}