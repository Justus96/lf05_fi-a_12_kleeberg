import java.util.Scanner;

public class Aufgabe_1 {
	public static void main(String[] args) {
		//aufgabe1();
		//aufgabe2();
		//aufgabe3();
		//aufgabe3_1();
		
	}
	public static void aufgabe1() {
		System.out.print("Das ist ein Beispielsatz. ");
		System.out.println("Ein Beispielsatz ist das.");
		// println hat einene Zeilenumbruch eingebaut, print nicht
		System.out.println("Er sagte: \"Guten Tag\"");
	}
	public static void aufgabe2() {
		String s = "*"	;
		System.out.printf("%6s\n", s );
		System.out.printf("%7s\n", s + s + s);
		System.out.printf("%8s\n", s + s + s + s + s);
		System.out.printf("%9s\n", s + s + s + s + s + s + s);
		System.out.printf("%10s\n", s + s + s + s + s + s + s + s + s);
		System.out.printf("%7s\n", s + s + s);
		System.out.printf("%7s\n", s + s + s);				
	}
	public static void aufgabe3() {
		double zahl1 = 22.4234234;
		double zahl2 = 111.2222;
		double zahl3 = 4.0;
		double zahl4 = 1000000.551;
		double zahl5 = 97.34;
		
		System.out.printf( "%.2f \n" , zahl1, -zahl1);
		System.out.printf( "%.2f \n" , zahl2, -zahl2);
		System.out.printf( "%.2f \n" , zahl3, -zahl3);
		System.out.printf( "%.2f \n" , zahl4, -zahl4);
		System.out.printf( "%.2f \n" , zahl5, -zahl5);
		
	}
	public static void aufgabe3_1() {
		System.out.println("Geben Sie eine double Zahl ein:");
		Scanner input = new Scanner(System.in);
		double num = input.nextDouble();
		System.out.printf( "%.2f \n" , num);	
		
	}
}