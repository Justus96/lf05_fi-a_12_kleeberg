import java.util.Scanner;
import java.lang.Math;

public class Fahrkartenarray {
    public static void main(String[] args)
    
    {
      
    	while(true) {
	       double zuZahlenderBetrag1 = fahrkatenbestellungErfassen();       
	       double r�ckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag1);
	
	       fahrkartenAusgeben();
	       rueckgeldAusgeben(r�ckgabebetrag);
    	}

    }
    
    public static double fahrkatenbestellungErfassen() {
    	Scanner myScanner = new Scanner(System.in);
    	int ticketNummer = 0;
    	int anzahlTickets = 0;
    	double zwischenSumme = 0;
    	double ticketPreis = 0;
    	while(ticketNummer != 9) {
	    	System.out.println("W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:");
	    	System.out.println("  Einzelfahrschein Regeltarif AB [3,00 EUR] (1)");
	    	System.out.println("  Tageskarte Regeltarif AB [8,80 EUR] (2)");
	    	System.out.println("  Kleingruppen-Tageskarte Regeltarif AB [25,50 EUR] (3)");
	    	System.out.println("  Bezahlen (9)");
	    	ticketNummer = inputInt();
	    	while((ticketNummer <= 0 || ticketNummer > 3) && ticketNummer != 9) {
		    		System.out.println(">>>falsche Eingabe<<<");
		    		ticketNummer = inputInt();
		    	}
		    	ticketPreis = 0;
		    	if(ticketNummer == 1) {
		    		ticketPreis = 3;
		    	}
		    	else if(ticketNummer == 2) {
		    		ticketPreis = 8.80;
		    	}
		    	else if(ticketNummer == 3) {
		    		ticketPreis = 25.50;
		    	}
		    	
		    	if(ticketNummer != 9) {
			    	System.out.println("Anzahl der Tickets: ");
			    	anzahlTickets = inputInt();
		    	}
			    	
		    	while (anzahlTickets <= 0 || anzahlTickets > 10) {
		    		System.out.println("Waehlen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
		    		anzahlTickets = inputInt();
		    	}
		    	zwischenSumme += ticketPreis*anzahlTickets;
		    	System.out.printf("Zwischensumme: " + "%,2f" + "\n", zwischenSumme);
    	}
    	double gesamtpreis = zwischenSumme;
    	return gesamtpreis;   	
    }
    public static double fahrkartenBezahlen(double zuZahlen) {
    	Scanner myScanner = new Scanner(System.in);
        double eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlen)
        {
     	   System.out.printf("Noch zu zahlen: " + "%.2f", (zuZahlen - eingezahlterGesamtbetrag), " Euro");
     	   System.out.print("\nEingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   double eingeworfeneM�nze = myScanner.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
        }
        double r�ckgabebetrag = (eingezahlterGesamtbetrag - zuZahlen);
        return r�ckgabebetrag;
    }
    public static void fahrkartenAusgeben() {
	    System.out.println("\nFahrschein wird ausgegeben");
	    for (int i = 0; i < 8; i++)
	    {
	       System.out.print("=");
	       warte(250);
	    }
	    System.out.println("\n\n");
    }
    public static void warte(int millisekunde) {
		  	try {
				Thread.sleep(millisekunde);
		  	} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	
    }
    public static void muenzeAusgeben(double rueckgabebetrag, String einheit) {
    	System.out.println(rueckgabebetrag + " " + einheit);
    }
    public static void rueckgeldAusgeben(double rueckgabebetrag) {
    	
        if(rueckgabebetrag > 0.0)
        {
     	   System.out.printf("Der R�ckgabebetrag in H�he von " + "%.2f", rueckgabebetrag);
     	   System.out.println(" Euro");
     	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while(rueckgabebetrag >= 1.99) // 2 EURO-M�nzen = 200 CENT
            {
         	  muenzeAusgeben(rueckgabebetrag, "Euro");
         	  rueckgabebetrag -= 2;
            }
            while(rueckgabebetrag >= 0.99) // 1 EURO-M�nzen = 100 CENT
            {
              muenzeAusgeben(rueckgabebetrag, "Euro");
         	  rueckgabebetrag -= 1;
            }
            while(rueckgabebetrag >= 0.49) // 50 CENT-M�nzen 
            {
              muenzeAusgeben(rueckgabebetrag, "Cent");
         	  rueckgabebetrag -= 0.5;
            }
            while(rueckgabebetrag >= 0.19) // 20 CENT-M�nzen 
            {
              muenzeAusgeben(rueckgabebetrag, "Cent");
         	  rueckgabebetrag -= 0.20;
            }
            while(rueckgabebetrag >= 0.09) // 10 CENT-M�nzen
            {
              muenzeAusgeben(rueckgabebetrag, "Cent");
         	  rueckgabebetrag -= 0.10;
            }
            while(rueckgabebetrag >= 0.04)// 5 CENT-M�nzen
            {
              muenzeAusgeben(rueckgabebetrag, "Cent");
         	  rueckgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir w�nschen Ihnen eine gute Fahrt.\n\n\n\n");
    }
    private static int inputInt() {
    	Scanner scn = new Scanner(System.in);
    	int input = 0;
    	boolean check = true;

    	while (check) {
    		try{
    			input = scn.nextInt();
    			check = false;
    			return input;
    		} catch (java.util.InputMismatchException | NumberFormatException e) {
    			System.out.println("Error: " + e);
    			scn.next();
    		}
    	}
    	return input;
    }
    
    
}