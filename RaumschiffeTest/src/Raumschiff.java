import java.util.ArrayList;
import java.util.Random;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * 
 * @author Kleeberg
 * @version V1.0
 *
 */

public class Raumschiff {

	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();

	public Raumschiff() {

	}

	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, String schiffsname, int androidenAnzah) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzah;
		this.schiffsname = schiffsname;

	}

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}

	public ArrayList<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}

	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzah) {
		this.androidenAnzahl = androidenAnzah;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	
	/**
	 * F�gt eine Ladung zum Ladungsverzeichnis hinzu
	 * 
	 * @param neueLadung Ladung, die geladen werden soll
	 */
	public void addLadung(Ladung neueLadung) {
		this.ladungsverzeichnis.add(neueLadung);
	}

	/**
	 * Schiesst einen Photonentorpedo auf ein Raumschiff r und vermerkt den Treffer
	 * Wenn kein Photonentorpedo vorhanden, wird die Nachricht "click" an alle gesendet
	 * 
	 * @param r Das Raumschiff, welches getroffen wird 
	 * 
	 */
	public void photonentorpedoSchiessen(Raumschiff r) {
		if (this.photonentorpedoAnzahl >= 1) {
			this.nachrichtAnAlle("Photonentorpedo abgeschossen!");
			this.photonentorpedoAnzahl -= 1;
			this.treffer(r);
		} else {
			this.nachrichtAnAlle("-=*Click*=-");
		}
	}

	/**
	 * Schiesst eine Phaserkanone auf ein Raumschiff r und vermerkt den Treffer
	 * Funktioniert nur, wenn die Energieversorgung gr��er als 50% ist
	 * 
	 * @param r Das Raumschiff, welches getroffen wird
	 */
	public void phaserkanoneSchiessen(Raumschiff r) {
		if (this.energieversorgungInProzent < 50) {
			this.nachrichtAnAlle("-=*Click*=-");
		} else {
			this.energieversorgungInProzent -= 50;
			this.nachrichtAnAlle("Phaserkanone abgeschossen");
			this.treffer(r);
		}
	}
	
	/**
	 * Vermerkt den Treffer bei einem Raumschiff r und reduziert die die Schilde um 50%
	 * Wenn die Schilde auf 0% sinken, werden H�lle und Energieversorgung um 50% reduziert
	 * Wenn die H�lle auf 0% sinkt, werden die Lebenserhaltungssysteme zerst�rt
	 * 
	 * @param r Das Raumschiff, welches getroffen wurde
	 */
	private void treffer(Raumschiff r) {
		System.out.println(r.schiffsname + " wurde getroffen!");

		r.schildeInProzent -= 50;
		if (r.schildeInProzent <= 0) {
			r.schildeInProzent = 0;
			r.huelleInProzent -= 50;
			r.energieversorgungInProzent -= 50;
		}

		if (r.huelleInProzent <= 0) {
			r.huelleInProzent = 0;
			r.lebenserhaltungssystemeInProzent = 0;
			System.out.println("Die Lebenserhaltungssysteme von " + r.schiffsname + " wurden vollst�ndig zerst�rt");
		}

	}
	
	/**
	 * Eine gegebene Nachricht wird ausgegeben und danach dem Broadcastkommunikator hinzugef�gt
	 * 
	 * @param message Die Nachricht, die an alle gesendet und dem BroadcastKommunikator hinzugef�gt wird
	 */
	public void nachrichtAnAlle(String message) {
		System.out.println("\nNachricht an alle:");
		System.out.println("\t" + message);
		this.broadcastKommunikator.add(message);
	}

	/**
	 * Gibt die ArrayListe des Broadcastkommunikators zur�ck
	 * 
	 * @return Der Broadcastkommunikator wird zur�ckgegeben
	 */
	public ArrayList<String> eintraegeLogbuchZurueckgeben() {
		return this.broadcastKommunikator;
	}
	
	/**
	 * Gibt das Logbuch zu einem bestimmten Raumschiff aus
	 * 
	 * @param logbuch das Logbuch, welches ausgegeben werden soll
	 */

	public void logbuchAusgeben(ArrayList<String> logbuch) {
		System.out.printf("\n%s\"%s\":\n", "Logbuch: ", this.schiffsname);
		for (String s : logbuch) {
			System.out.println("\t" + s);
		}
	}

	/**
	 * L�dt eine bestimmte Anzahl an Photonentorpedos, wenn sie sich im Ladungsverzeichnisses befinden
	 * Wenn sich keine Photonentorpedos im Ladungsverzeichnis befinden, werden auch keine geladen
	 * Wenn mehr Torpedos geladen werden sollen, als sich im Verzeichnis befinden, wird nur die Menge geladen, die vorhanden ist
	 * 
	 * @param anzahlTorpedos Die Anzahl der Torpedos, die geladen werden sollen
	 */
	public void photonentorpedosLaden(int anzahlTorpedos) {
		boolean check = false;

		for (int i = 0; i < this.getLadungsverzeichnis().size(); i++) {
			if (this.getLadungsverzeichnis().get(i).getBezeichnung().toLowerCase().equals("photonentorpedo")) {
				if (this.getLadungsverzeichnis().get(i).getMenge() >= anzahlTorpedos) {
					check = true;
					this.photonentorpedoAnzahl = anzahlTorpedos;
					int neueMenge = this.getLadungsverzeichnis().get(i).getMenge() - anzahlTorpedos;
					this.getLadungsverzeichnis().get(i).setMenge(neueMenge);
					System.out.println(this.photonentorpedoAnzahl + " Photonentorpedo(s) eingesetzt");
					break;
				}
			}

		}

		if (check) {
		} else {
			System.out.println("Keine Photonentorpedos gefunden!");
			this.nachrichtAnAlle("-=*Click*=-");
		}
	}

	/**
	 * Repariert das Schiff, anhand von verschiedenen Parametern
	 * Eine Zufallszahl beeinflusst die Reparatur, weiterhin beeinflusst die Anzahl an eingesetzten
	 * Reparaturandroiden die Reparatur. Sollten mehr eingesetzt werden als vorhanden sind, dann 
	 * wird die maximal vorhandene Anzahl eingesetzt.
	 * 
	 * @param schutzschilde Prozentuale Kapazit�t der Schutzschilde
	 * @param energieversorgung Prozentuale Kapazit�t der Energieversorgung
	 * @param schiffshuelle Prozentuale Kapazit�t der Schiffsh�lle
	 * @param anzahlDroiden Anzahl der Reperatursandroiden, welche sich an Board befinden
	 */
	public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle,
			int anzahlDroiden) {

		int zuReparieren = 0;
		if (schutzschilde)
			zuReparieren++;
		if (energieversorgung)
			zuReparieren++;
		if (schiffshuelle)
			zuReparieren++;

		// Generieren der Zufallszahl
		Random random = new Random();
		int r = random.nextInt(100 + 1) + 1;

		if (anzahlDroiden > this.androidenAnzahl) {
			anzahlDroiden = this.androidenAnzahl;
		}

		if (zuReparieren != 0) {
			double reparaturInProzent = (r * anzahlDroiden) / zuReparieren;

			if (schutzschilde)
				this.schildeInProzent += reparaturInProzent;
			if (energieversorgung)
				this.energieversorgungInProzent += reparaturInProzent;
			if (schiffshuelle)
				this.huelleInProzent += reparaturInProzent;
		}

	}

	/**
	 * Gibt die einzelenen Zust�nde des Raumschiffes formatiert aus. 
	 */
	public void zustandRaumschiff() {
		System.out.println("\nDas Schiff hei�t " + this.schiffsname);
		System.out.println("Die Anzahl der Photonentorpedos betr�gt: " + this.photonentorpedoAnzahl);
		System.out.println("Die Energieversorgung betr�gt " + this.energieversorgungInProzent + "%");
		System.out.println("Die Schildst�rke betr�gt " + this.schildeInProzent + "%");
		System.out.println("Die Huelle hat eine St�rke von " + this.huelleInProzent + "%");
		System.out.println(
				"Die Lebenserhaltungssysteme haben eine St�rke von " + this.lebenserhaltungssystemeInProzent + "%");
		System.out.println("Das Raumschiff hat " + this.androidenAnzahl + " Androiden");

	}

	/**
	 * Gibt nacheinander die Inhalte des Ladungsverzeichnisses aus
	 */
	public void ladungsverzeichnisAusgeben() {
		System.out.println("\nLadungsverzeichnis: ");
		for (Ladung l : this.getLadungsverzeichnis()) {
			System.out.println(l);
		}
	}

	/**
	 * L�scht Ladungen aus dem Ladungsverzeichnis, wenn die Menge des Objekts im Verzeichnis 0 betr�gt
	 */
	public void ladungsverzeichnisAufraeumen() {
		for (int i = 0; i < this.getLadungsverzeichnis().size(); i++) {
			if (this.getLadungsverzeichnis().get(i).getMenge() == 0) {
				this.getLadungsverzeichnis().remove(i);
			}
		}
	}

}
