//@ Author: Justus Kleeberg
//  Date: 19.09.2021

public class Konsolenausgabe2 {

	public static void main(String[] args) {
		//Aufgabe1();		//Kommentierung der Aufgaben wegmachen, um die jeweilige Aufgabe auszuführe
		//Aufgabe2();
		//Aufgabe3();		//veraltete Version, vor dem 17.09. gemacht
		//Aufgabe3_1();		//aktuelle Version, mit neuem Wissen vom 17.09.
	}
	public static void Aufgabe1() {
		String s = "*";
		System.out.printf("%6s\n", s + s);
		System.out.printf("%2s", s);
		System.out.printf("%7s\n", s);
		System.out.printf("%2s", s);
		System.out.printf("%7s\n", s);
		System.out.printf("%6s\n", s + s);
	}
	public static void Aufgabe2() {
		System.out.print("0!");
		System.out.printf("%5s", "=");
		System.out.printf("%19s","");
		System.out.print("=");
		System.out.printf("%5d \n", 1);
		
		System.out.print("1!");
		System.out.printf("%5s", "=");
		System.out.printf("%-19s"," 1");
		System.out.print("=");
		System.out.printf("%5d \n", 1);
		
		System.out.print("2!");
		System.out.printf("%5s", "=");
		System.out.printf("%-19s"," 1*2");
		System.out.print("=");
		System.out.printf("%5d \n", 2);

		System.out.print("3!");
		System.out.printf("%5s", "=");
		System.out.printf("%-19s"," 1*2*3");
		System.out.print("=");
		System.out.printf("%5d \n", 6);
		
		System.out.print("4!");
		System.out.printf("%5s", "=");
		System.out.printf("%-19s"," 1*2*3*4");
		System.out.print("=");
		System.out.printf("%5d \n", 24);

		System.out.print("5!");
		System.out.printf("%5s", "=");
		System.out.printf("%-19s"," 1*2*3*4*5");
		System.out.print("=");
		System.out.printf("%5d \n", 120);
		
	}

	public static void Aufgabe3() {
		System.out.printf("%-12s", "Fahrenheit");
		System.out.print("|");
		System.out.printf("%10s \n", "Celsius");
		
		System.out.printf("------------------------ \n");
		
		System.out.printf("%-12d", -20);
		System.out.print("|");
		System.out.printf("%10.2f \n", -28.89);
		
		System.out.printf("%-12d", -10);
		System.out.print("|");
		System.out.printf("%10.2f \n", -23.33);
		
		System.out.printf("%-12d", +0);
		System.out.print("|");
		System.out.printf("%10.2f \n", -17.78);
		
		System.out.printf("%-12d", +10);
		System.out.print("|");
		System.out.printf("%10.2f \n", -6.67);
		
		System.out.printf("%-12d", +20);
		System.out.print("|");
		System.out.printf("%10.2f \n", -1.11);
				
	}
	
	public static void Aufgabe3_1() {
		
		System.out.printf("%-12s|", "Fahrenheit");
        System.out.printf("%10s\n", "Celsius");
        System.out.println("-----------------------");  
        System.out.printf("%+-12d|", -20);
        System.out.printf("%10.2f\n", -28.89);
        System.out.printf("%+-12d|", -10);
        System.out.printf("%10.2f\n", -23.33);
        System.out.printf("%+-12d|", 0);
        System.out.printf("%10.2f\n", -17.78);
        System.out.printf("%+-12d|", +10);
        System.out.printf("%10.2f\n", -6.67);
        System.out.printf("%+-12d|", +20);
        System.out.printf("%10.2f\n", -1.11);
	}
	

}
