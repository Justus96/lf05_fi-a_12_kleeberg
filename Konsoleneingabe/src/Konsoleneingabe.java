/** 
    @Justus
    @06.10.2021
*/

import java.util.Scanner; // Import der Klasse Scanner 
 
public class Konsoleneingabe 
{ 
   
  public static void main(String[] args) { 
	  
	  // Aufgabe1();
	  // Aufgabe2();
     
  }   
  
  public static void Aufgabe1() {
	     
	    // Neues Scanner-Objekt myScanner wird erstellt     
	    Scanner myScanner = new Scanner(System.in);  
	     
	    System.out.print("Bitte geben Sie eine ganze Zahl ein: ");    
	     
	    // Die Variable zahl1 speichert die erste Eingabe 
	    int zahl1 = myScanner.nextInt();  
	     
	    System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: "); 
	     
	    // Die Variable zahl2 speichert die zweite Eingabe 
	    int zahl2 = myScanner.nextInt();  
	     
	    // Die Addition der Variablen zahl1 und zahl2  
	    // wird der Variable ergebnis zugewiesen. 
	    int summe = zahl1 + zahl2; 
	    int diff = zahl1 - zahl2;
	    int prod = zahl1*zahl2;
	    double quot = zahl1/zahl2;
	     
	    System.out.print("\n\n\nErgebnis der Addition (Summe) lautet: "); 
	    System.out.print(zahl1 + " + " + zahl2 + " = " + summe);  
	 
	    System.out.print("\n\n\nErgebnis der Subtraktion (Differenz) lautet: "); 
	    System.out.print(zahl1 + " - " + zahl2 + " = " + diff);  
	    
	    System.out.print("\n\n\nErgebnis der Multiplikation (Produkt) lautet: "); 
	    System.out.print(zahl1 + " * " + zahl2 + " = " + prod);  
	    
	    System.out.print("\n\n\nErgebnis der Division (Quotient) lautet: "); 
	    System.out.print(zahl1 + " / " + zahl2 + " = " + quot);  
	        
	 
	    myScanner.close(); 	  
	  
  }
  
  public static void Aufgabe2() {
	  
	  System.out.println("Guten Tag! Kann ich Ihren Namen und Ihr Alter haben?");
	  System.out.println("Zuerst m�chte ich Ihren Namen haben, geben Sie diesen nun ein:");
	  Scanner myScanner = new Scanner(System.in); 
	  String user_name = myScanner.nextLine();
	  System.out.println("Jetzt ben�tige ich noch Ihr Alter:");
	  int alter = myScanner.nextInt();
	  System.out.println("Guten Tag " + user_name + ". Sie sind " + alter + " Jahre alt.");
	  
	  
	  
	  
  }
}