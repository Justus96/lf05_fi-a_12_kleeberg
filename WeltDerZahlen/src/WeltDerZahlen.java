/**

  @version 1.0 from 17.09.2021
  @author << Justus Kleeberg >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten =  8;
    
    // Anzahl der Sterne in unserer Milchstra�e
    int anzahlSterne = 250;  
    
    // Wie viele Einwohner hat Berlin?
    int bewohnerBerlin = 3645000;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
    int meinAlter = 25;
    int alterTage = 9369;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
     int gewichtKilogramm = 150000;
    
    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
     int flaecheGroessteLand = 17130000;
    
    // Wie gro� ist das kleinste Land der Erde?
    
     int flaecheKleinsteLand = 44;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne in Milliardden: "+ anzahlSterne);
    
    System.out.println("Anzahl der Bewohner Berlins: " + bewohnerBerlin);
    
    System.out.println("Ich bin " + meinAlter + " Jahre alt. Das sind so viele Tage: " + alterTage);
    
    System.out.println("Gewicht des schwersten Tieres der Welt (dem Blauwal): " + gewichtKilogramm);
    
    System.out.println("Fl�che des gr��ten Lands der Welt in km^2 (Russland): " + flaecheGroessteLand);
    
    System.out.println("Fl�che des kleinsten Lands der Welt in Hektar (Vatikanstadt): " + flaecheKleinsteLand);
    
    
    
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}
