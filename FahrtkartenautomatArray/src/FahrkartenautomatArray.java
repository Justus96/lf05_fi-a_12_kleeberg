import java.util.Scanner;
import java.lang.Math;

public class FahrkartenautomatArray {
    public static void main(String[] args)
    
    {    
    	while(true) {
	       double zuZahlenderBetrag1 = fahrkatenbestellungErfassen();       
	       double r�ckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag1);
	
	       fahrkartenAusgeben();
	       rueckgeldAusgeben(r�ckgabebetrag);
    	}
    }
    
    public static double fahrkatenbestellungErfassen() {
    	Scanner myScanner = new Scanner(System.in);
    	int anzahlTickets = 0;
    	int ticketNummer = 1;
    	double zwischenSumme = 0;
    	double ticketPreis = 0;
    	boolean schleife = true;
    	
    	String tickets[] = {"Einzelfahrschein AB", "Einzelfahrschein BC", "Einzelfahrschein ABC",
    			"Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABD",
    			"Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC",
    			"Kleingruppen-Tageskarte Berlin ABC"};
    	double preis[] = {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
    	
        System.out.printf(" %s \t\t\t%-34s %s\n", "Auswahlnummer", "Bezeichnung", "Preis in Euro");
        System.out.println("----------------------------------------------------------------------------------");
        for(int i = 0; i<tickets.length;i++) {
            System.out.printf(" %d \t\t\t%-34s \t\t%.2f\n", i+1, tickets[i], preis[i]);
            System.out.println("----------------------------------------------------------------------------------");
        }
    	  	
    	while(schleife) {
    		System.out.println("Waehlen Sie welches Ticket Sie haben m�chten: ");
    		System.out.println("Druecken Sie die 0 um den Kaufvorgang zu beenden und zu bezahlen.");
    		ticketNummer = inputInt();
    		while((ticketNummer <= 0 || ticketNummer > 10) && ticketNummer != 0) {
	    		System.out.println(">>>" + ticketNummer +" ist eine falsche Eingabe<<<");
	    		ticketNummer = inputInt();
    		}
    	
	    	if(ticketNummer > 0) {	
		    	ticketPreis = preis[ticketNummer-1];
		    	System.out.println("Anzahl der Tickets: ");
		    	anzahlTickets = inputInt();
			    while (anzahlTickets <= 0 || anzahlTickets > 10) {
			    	System.out.println("Waehlen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
			    	anzahlTickets = inputInt();
			    }
			    zwischenSumme += ticketPreis*anzahlTickets;
			    System.out.printf("Zwischensumme: " + "%.2f" + "\n", zwischenSumme);
	    	}
	    	else {
	    		ticketPreis = 0;
	    		schleife = false;
	    	}

    	}
    		
	    double gesamtPreis = zwischenSumme;
	    return gesamtPreis;	
    	
    }    	
        
    public static double fahrkartenBezahlen(double zuZahlen) {
    	Scanner myScanner = new Scanner(System.in);
        double eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlen)
        {
     	   System.out.printf("Noch zu zahlen: " + "%.2f", (zuZahlen - eingezahlterGesamtbetrag), " Euro");
     	   System.out.print("\nEingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   double eingeworfeneM�nze = myScanner.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
        }
        double r�ckgabebetrag = (eingezahlterGesamtbetrag - zuZahlen);
        return r�ckgabebetrag;
    }
    public static void fahrkartenAusgeben() {
	    System.out.println("\nFahrschein wird ausgegeben");
	    for (int i = 0; i < 8; i++)
	    {
	       System.out.print("=");
	       warte(250);
	    }
	    System.out.println("\n\n");
    }
    public static void warte(int millisekunde) {
		  	try {
				Thread.sleep(millisekunde);
		  	} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	
    }
    public static void muenzeAusgeben(double rueckgabebetrag, String einheit) {
    	System.out.printf("%.2f" + " " + einheit + "\n", rueckgabebetrag);
    }
    public static void rueckgeldAusgeben(double rueckgabebetrag) {
    	
        if(rueckgabebetrag > 0.0)
        {
     	   System.out.printf("Der R�ckgabebetrag in H�he von " + "%.2f", rueckgabebetrag);
     	   System.out.println(" Euro");
     	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while(rueckgabebetrag >= 1.99) // 2 EURO-M�nzen = 200 CENT
            {
         	  muenzeAusgeben(2.0, "Euro");
         	  rueckgabebetrag -= 2.0;
            }
            while(rueckgabebetrag >= 0.99) // 1 EURO-M�nzen = 100 CENT
            {
              muenzeAusgeben(1.0, "Euro");
         	  rueckgabebetrag -= 1.0;
            }
            while(rueckgabebetrag >= 0.49) // 50 CENT-M�nzen 
            {
              muenzeAusgeben(0.5, "Cent");
         	  rueckgabebetrag -= 0.5;
            }
            while(rueckgabebetrag >= 0.19) // 20 CENT-M�nzen 
            {
              muenzeAusgeben(0.2, "Cent");
         	  rueckgabebetrag -= 0.20;
            }
            while(rueckgabebetrag >= 0.09) // 10 CENT-M�nzen
            {
              muenzeAusgeben(0.1, "Cent");
         	  rueckgabebetrag -= 0.10;
            }
            while(rueckgabebetrag >= 0.04)// 5 CENT-M�nzen
            {
              muenzeAusgeben(0.05, "Cent");
         	  rueckgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir w�nschen Ihnen eine gute Fahrt.\n\n\n\n");
    }
    private static int inputInt() {
    	Scanner scn = new Scanner(System.in);
    	int input = 0;
    	boolean check = true;

    	while (check) {
    		try{
    			input = scn.nextInt();
    			check = false;
    			return input;
    		} catch (java.util.InputMismatchException | NumberFormatException e) {
    			System.out.println("Error: " + e + "\nBitte gib einen gueltigen Wert ein.");
    			scn.next();
    		}
    	}
    	return input;
    }
    
    
}