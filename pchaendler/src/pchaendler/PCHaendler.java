package pchaendler;
import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		String artikel = liesArtikel("Welchen Artikel m�chten Sie bestellen?");

		int anzahl = liesAnzahl("Wie haeufig moechten Sie den Artikel bestellen?");

		double nettopreis = liesNettopreis("Geben Sie den Nettopreis ein: ");
		
		double mwst = liesMehrwertsteuer("Geben Sie die Mehrwertsteuer in Prozent ein: ");
		
		double nettogesamtpreis = berechneGesamtnettoopreis(anzahl, nettopreis);
		
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);
		
		rechnungAusgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);

	}	
	public static String liesArtikel(String eingabetext) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(eingabetext);
		String artikel = myScanner.next();
		return artikel;		
	}	
	public static int liesAnzahl(String eingabetext) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(eingabetext);
		int anzahl = myScanner.nextInt();
		return anzahl;				
	}	
	public static double liesNettopreis(String eingabetext) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(eingabetext);
		double nettopreis = myScanner.nextDouble();
		return nettopreis;				
	}	
	public static double liesMehrwertsteuer(String eingabetext) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(eingabetext);
		double mwst = myScanner.nextInt();
		return mwst;			
	}
	public static double berechneGesamtnettoopreis(int anzahl, double nettopreis) {
		double nettogesamtpreis = anzahl * nettopreis;
		return nettogesamtpreis;		
	}
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
		return bruttogesamtpreis;
	}
	public static void rechnungAusgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}

}