import java.util.Scanner;
import java.lang.Math;

public class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double ticketPreis;
       double eingezahlterGesamtbetrag; // F�r alle au�er anzahlTickets: sind Kommazahlen, deswegen double
       double eingeworfeneM�nze;
       double r�ckgabebetrag;
       int anzahlTickets; // Anzahl der Tickets ist ein Int, da man nur eine ganze Zahl an Tickets kaufen kann

       
       System.out.print("Ticketpreis in EURO: ");
       ticketPreis = tastatur.nextDouble();
       System.out.print("Anzahl der Tickets: ");
       anzahlTickets = tastatur.nextInt();  
       
       zuZahlenderBetrag = ticketPreis * anzahlTickets;
       // Die Variable zuZahlenderBetrag ist gleich mit dem ticketPreis multipliziert mit der anzahlTickets Varibale.
       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   System.out.printf("Noch zu zahlen: " + "%.2f", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
    	   System.out.print("\nEingabe (mind. 5Ct, h�chstens 2 Euro): ");
    	   eingeworfeneM�nze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
       }

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // R�ckgeldberechnung und -Ausgabe
       // -------------------------------
       r�ckgabebetrag = (eingezahlterGesamtbetrag - zuZahlenderBetrag)*100; //Umrechnung in Cent
       if(r�ckgabebetrag > 0.0)
       {
    	   System.out.printf("Der R�ckgabebetrag in H�he von " + "%.2f", r�ckgabebetrag/100);
    	   System.out.println(" Euro");
    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

           while(r�ckgabebetrag >= 200) // 2 EURO-M�nzen = 200 CENT
           {
        	  System.out.println("2 EURO");
	          r�ckgabebetrag -= 200;
           }
           while(r�ckgabebetrag >= 100) // 1 EURO-M�nzen = 100 CENT
           {
        	  System.out.println("1 EURO");
	          r�ckgabebetrag -= 100;
           }
           while(r�ckgabebetrag >= 50) // 50 CENT-M�nzen 
           {
        	  System.out.println("50 CENT");
	          r�ckgabebetrag -= 50;
           }
           while(r�ckgabebetrag >= 20) // 20 CENT-M�nzen 
           {
        	  System.out.println("20 CENT");
 	          r�ckgabebetrag -= 20;
           }
           while(r�ckgabebetrag >= 10) // 10 CENT-M�nzen
           {
        	  System.out.println("10 CENT");
	          r�ckgabebetrag -= 10;
           }
           while(r�ckgabebetrag >= 5)// 5 CENT-M�nzen
           {
        	  System.out.println("5 CENT");
 	          r�ckgabebetrag -= 5;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
    }
}